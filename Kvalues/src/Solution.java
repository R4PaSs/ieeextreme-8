import java.util.*;

public class Solution {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		int nval = sc.nextInt();
		int mlen = sc.nextInt(); // Moving window size
		int kval = sc.nextInt(); // K-th smallest value in window

		int[] values = new int[nval];

		ArrayDeque<Integer> window = new ArrayDeque<Integer>();
		PriorityQueue<Integer> minimums = new PriorityQueue<Integer>();

		for (int i = 0; i < values.length; i++) {
			values[i] = sc.nextInt();
		}

		sc.close();

		// Iterate over values
		for (int i = 0; i < values.length; i++) {

			// Fill window
			if (window.size() < mlen-1) {
				for (int j = 0; window.size() < mlen; j++) {
					window.add(values[(i + j) % values.length]);
				}				
			} else {
				window.add(values[(i + mlen - 1) % values.length]);
			}

			// Check for k-value
			PriorityQueue<Integer> sortedWindow = new PriorityQueue<Integer>(
					window);
			int min = Integer.MAX_VALUE;
			for (int pop = 0; pop < kval; pop++) {
				min = sortedWindow.poll();
			}
			minimums.add(min);

			window.removeFirst();

		}

		System.out.println(minimums.peek());
	}

}

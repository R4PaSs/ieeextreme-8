import java.util.*;

// Assembly for Xtreme-8000 processor
class Solution {

	public static class ASMMachine {

		int ip = 0;

		int cmpreg = 0;

		public int[] memory;

		public Map<String, Integer> labels = new HashMap<String, Integer>();

		public ASMMachine(int size){
			memory = new int[size];
			for(int i = 0; i < size; i++){
				memory[i] = 0;
			}
		}

		public void exec(Command c){
			ip += 1;
			c.eval(this);
		}
	}

	abstract public static class Command{
		abstract public void eval(ASMMachine v);
	}

	public static class PrintReg extends Command {
		int reg;

		public void eval(ASMMachine v){
			int cell = v.memory[reg];
			if(cell < 16)
				System.out.print("0");
			System.out.print(get_hex(cell));
			System.out.print("\n");
		}
	}

	public static class PrintRegRange extends Command {
		int freg;
		int treg;

		public void eval(ASMMachine v){
			for(int i = freg; i < treg; i++){
				int cell = v.memory[i];
				if(cell < 16)
					System.out.print("0");
				System.out.print(get_hex(cell));
				System.out.print(" ");
			}
			int cell = v.memory[treg];
			if(cell < 16)
				System.out.print("0");
			System.out.print(get_hex(cell));
			System.out.print("\n");
		}
	}

	public static class MoveCstReg extends Command {
		int val;
		int reg;

		public void eval(ASMMachine v){
			v.memory[reg] = val;
		}
	}

	public static class MoveCstDeref extends Command {
		int val;
		int deref;

		public void eval(ASMMachine v){
			v.memory[v.memory[deref]] = val;
		}
	}

	public static class MoveDerefReg extends Command{
		int deref;
		int reg;

		public void eval(ASMMachine v){
			v.memory[reg] = v.memory[v.memory[deref]];
		}
	}

	public static class MoveDerefDeref extends Command {
		int ld;
		int rd;

		public void eval(ASMMachine v){
			v.memory[v.memory[rd]] = v.memory[v.memory[ld]];
		}
	}

	public static class MoveRegDeref extends Command{
		int ld;
		int rd;

		public void eval(ASMMachine v){
			v.memory[v.memory[rd]] = v.memory[ld];
		}
	}

	public static class MoveRegReg extends Command {
		int ld;
		int rd;

		public void eval(ASMMachine v){
			v.memory[rd] = v.memory[ld];
		}
	}

	public static class AddRegReg extends Command{
		int ld;
		int rd;

		public void eval(ASMMachine v){
			v.memory[rd] += v.memory[ld];
			v.memory[rd] = 0x000000FF & v.memory[rd];
		}
	}

	public static class AddCstReg extends Command {
		int ld;
		int rd;

		public void eval(ASMMachine v){
			v.memory[rd] += ld;
			v.memory[rd] = 0x000000FF & v.memory[rd];
		}
	}

	public static class SubRegReg extends Command {
		int ld;
		int rd;

		public void eval(ASMMachine v){
			v.memory[rd] -= v.memory[ld];
			v.memory[rd] = 0x000000FF & v.memory[rd];
		}
	}

	public static class SubCstReg extends Command {
		int ld;
		int rd;

		public void eval(ASMMachine v){
			v.memory[rd] -= ld;
			v.memory[rd] = 0x000000FF & v.memory[rd];
		}
	}

	public static class AndCstReg extends Command {
		int ld;
		int rd;

		public void eval(ASMMachine v){
			v.memory[rd] = v.memory[rd] & ld;
		}
	}

	public static class AndRegReg extends Command {
		int ld;
		int rd;

		public void eval(ASMMachine v){
			v.memory[rd] = v.memory[ld] & v.memory[rd];
		}
	}

	public static class OrCstReg extends Command{
		int ld;
		int rd;

		public void eval(ASMMachine v){
			v.memory[rd] = ld | v.memory[rd];
		}
	}

	public static class OrRegReg extends Command{
		int ld;
		int rd;

		public void eval(ASMMachine v){
			v.memory[rd] = v.memory[ld] | v.memory[rd];
		}
	}

	public static class XorCstReg extends Command{
		int ld;
		int rd;

		public void eval(ASMMachine v){
			v.memory[rd] = ld ^ v.memory[rd];
		}
	}

	public static class XorRegReg extends Command{
		int ld;
		int rd;

		public void eval(ASMMachine v){
			v.memory[rd] = v.memory[ld] ^ v.memory[rd];
		}
	}

	public static class CompCstReg extends Command{
		int ld;
		int rd;

		public void eval(ASMMachine v){
			int rval = v.memory[rd];
			int lval = ld;
			v.cmpreg = (new Integer(lval)).compareTo(rval);
		}
	}

	public static class CompRegReg extends Command{
		int ld;
		int rd;

		public void eval(ASMMachine v){
			int rval = v.memory[rd];
			int lval = v.memory[ld];
			v.cmpreg = (new Integer(lval)).compareTo(rval);
		}
	}

	public static class BeqLbl extends Command{
		String lbl;

		public void eval(ASMMachine v){
			if(v.cmpreg == 0)
				v.ip = v.labels.get(lbl);
		}
	}

	public static class BneLbl extends Command{
		String lbl;

		public void eval(ASMMachine v){
			if(v.cmpreg != 0)
				v.ip = v.labels.get(lbl);
		}
	}

	public static class BgtLbl extends Command{
		String lbl;

		public void eval(ASMMachine v){
			if(v.cmpreg == 1)
				v.ip = v.labels.get(lbl);
		}
	}

	public static class BgeLbl extends Command{
		String lbl;

		public void eval(ASMMachine v){
			if(v.cmpreg == 0 || v.cmpreg == 1)
				v.ip = v.labels.get(lbl);
		}
	}

	public static class BltLbl extends Command{
		String lbl;

		public void eval(ASMMachine v){
			if(v.cmpreg == -1)
				v.ip = v.labels.get(lbl);
		}
	}

	public static class BleLbl extends Command{
		String lbl;

		public void eval(ASMMachine v){
			if(v.cmpreg == 0 || v.cmpreg == -1)
				v.ip = v.labels.get(lbl);
		}
	}

	public static String get_hex(int i){
		return Integer.toString(i, 16).toUpperCase();
	}

	public static int parse_hex(String s){
		int quo = 1;
		int total = 0;
		for(int i = s.length() - 1; i >= 0; i--){
			total += quo * char_to_int(s.charAt(i));
			quo = quo * 16;
		}
		return total;
	}

	public static int char_to_int(char c){
		if(c >= '0' && c <= '9')
			return c - '0';
		if(c >= 'A' && c <= 'F')
			return c - 55;
		if(c >= 'a' && c <= 'f')
			return c - 87;
		return -1;
	}

	public static Command parse_cmd(String s){
		String[] pts = s.trim().split(" ");
		String[] parts = clean_parts(pts);
		if(parts.length == 0){
			return null;
		}
		if(isLowerCase(parts[0])){
			String[] ptts = new String[parts.length - 1];
			int j = 0;
			for(int i = 1; i < parts.length; i++){
				ptts[j] = parts[i];
				j++;
			}
			parts = ptts;
		}
		for(int i = 0; i < parts.length; i++){
			if(parts[i].equals("PRINT")){
				if(parts.length == 2){
					PrintReg ret = new PrintReg();
					ret.reg = parse_hex(parts[1]);
					return ret;
				} else {
					PrintRegRange ret = new PrintRegRange();
					ret.freg = parse_hex(parts[1]);
					ret.treg = parse_hex(parts[2]);
					return ret;
				}
			} else if(parts[i].equals("MOVE")){
				String lft = parts[1];
				String rgt = parts[2];
				if(is_reg(lft)){
					if(is_reg(rgt)){
						MoveRegReg ret = new MoveRegReg();
						ret.ld = parse_hex(lft);
						ret.rd = parse_hex(rgt);
						return ret;
					}else if(is_deref(rgt)){
						MoveRegDeref ret = new MoveRegDeref();
						ret.ld = parse_hex(lft);
						ret.rd = parse_hex(strip_st(rgt));
						return ret;
					}
				} else if(is_cst(lft)){
					if(is_reg(rgt)){
						MoveCstReg ret = new MoveCstReg();
						ret.val = parse_hex(strip_st(lft));
						ret.reg = parse_hex(rgt);
						return ret;
					}else if(is_deref(rgt)){
						MoveCstDeref ret = new MoveCstDeref();
						ret.val = parse_hex(strip_st(lft));
						ret.deref = parse_hex(strip_st(rgt));
						return ret;
					}
				} else if(is_deref(lft)){
					if(is_reg(rgt)){
						MoveDerefReg ret = new MoveDerefReg();
						ret.deref = parse_hex(strip_st(lft));
						ret.reg = parse_hex(rgt);
						return ret;
					}else if(is_deref(rgt)){
						MoveDerefDeref ret = new MoveDerefDeref();
						ret.ld = parse_hex(strip_st(lft));
						ret.rd = parse_hex(strip_st(rgt));
						return ret;
					}
				}
			} else if(parts[i].equals("ADD")){
				String lft = parts[1];
				String rgt = parts[2];
				if(is_cst(lft)){
					AddCstReg ret = new AddCstReg();
					ret.ld = parse_hex(strip_st(lft));
					ret.rd = parse_hex(rgt);
					return ret;
				}else if(is_reg(lft)){
					AddRegReg ret = new AddRegReg();
					ret.ld = parse_hex(lft);
					ret.rd = parse_hex(rgt);
					return ret;
				}
			} else if(parts[i].equals("SUB")){
				String lft = parts[1];
				String rgt = parts[2];
				if(is_cst(lft)){
					SubCstReg ret = new SubCstReg();
					ret.ld = parse_hex(strip_st(lft));
					ret.rd = parse_hex(rgt);
					return ret;
				}else if(is_reg(lft)){
					SubRegReg ret = new SubRegReg();
					ret.ld = parse_hex(lft);
					ret.rd = parse_hex(rgt);
					return ret;
				}
			} else if(parts[i].equals("AND")){
				String lft = parts[1];
				String rgt = parts[2];
				if(is_cst(lft)){
					AndCstReg ret = new AndCstReg();
					ret.ld = parse_hex(strip_st(lft));
					ret.rd = parse_hex(rgt);
					return ret;
				}else if(is_reg(lft)){
					AndRegReg ret = new AndRegReg();
					ret.ld = parse_hex(lft);
					ret.rd = parse_hex(rgt);
					return ret;
				}
			} else if(parts[i].equals("OR")){
				String lft = parts[1];
				String rgt = parts[2];
				if(is_cst(lft)){
					OrCstReg ret = new OrCstReg();
					ret.ld = parse_hex(strip_st(lft));
					ret.rd = parse_hex(rgt);
					return ret;
				}else if(is_reg(lft)){
					OrRegReg ret = new OrRegReg();
					ret.ld = parse_hex(lft);
					ret.rd = parse_hex(rgt);
					return ret;
				}
			} else if(parts[i].equals("XOR")){
				String lft = parts[1];
				String rgt = parts[2];
				if(is_cst(lft)){
					XorCstReg ret = new XorCstReg();
					ret.ld = parse_hex(strip_st(lft));
					ret.rd = parse_hex(rgt);
					return ret;
				}else if(is_reg(lft)){
					XorRegReg ret = new XorRegReg();
					ret.ld = parse_hex(lft);
					ret.rd = parse_hex(rgt);
					return ret;
				}
			} else if(parts[i].equals("COMP")){
				String lft = parts[1];
				String rgt = parts[2];
				if(is_cst(lft)){
					CompCstReg ret = new CompCstReg();
					ret.ld = parse_hex(strip_st(lft));
					ret.rd = parse_hex(rgt);
					return ret;
				}else if(is_reg(lft)){
					CompRegReg ret = new CompRegReg();
					ret.ld = parse_hex(lft);
					ret.rd = parse_hex(rgt);
					return ret;
				}
			} else if(parts[i].equals("BEQ")){
				BeqLbl ret = new BeqLbl();
				ret.lbl = parts[i+1];
				return ret;
			} else if(parts[i].equals("BNE")){
				BneLbl ret = new BneLbl();
				ret.lbl = parts[i+1];
				return ret;
			} else if(parts[i].equals("BGT")){
				BgtLbl ret = new BgtLbl();
				ret.lbl = parts[i+1];
				return ret;
			} else if(parts[i].equals("BGE")){
				BgeLbl ret = new BgeLbl();
				ret.lbl = parts[i+1];
				return ret;
			} else if(parts[i].equals("BLT")){
				BltLbl ret = new BltLbl();
				ret.lbl = parts[i+1];
				return ret;
			} else if(parts[i].equals("BLE")){
				BleLbl ret = new BleLbl();
				ret.lbl = parts[i+1];
				return ret;
			}
		}
		return null;
	}

	public static String strip_st(String s){
		StringBuffer sb = new StringBuffer();
		for(int i = 0; i < s.length(); i++){
			char c = s.charAt(i);
			if(!is_hex_digit(c))
				continue;
			sb.append(c);
		}
		return sb.toString();
	}

	public static boolean is_reg(String s){
		for(int i = 0; i < s.length(); i++){
			if(!is_hex_digit(s.charAt(i)))
				return false;
		}
		return true;
	}

	public static boolean is_cst(String s){
		if(s.charAt(0) != '#')
			return false;
		for(int i = 1; i < s.length(); i++){
			if(!is_hex_digit(s.charAt(i)))
				return false;
		}
		return true;
	}

	public static boolean is_deref(String s){
		if(s.charAt(0) != '(')
			return false;
		if(s.charAt(s.length()-1) != ')')
			return false;
		for(int i = 1; i < s.length() -1; i++){
			if(!is_hex_digit(s.charAt(i)))
				return false;
		}
		return true;
	}

	public static boolean is_hex_digit(char c){
		return ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f'));
	}

	public static String[] clean_parts(String[] s){
		ArrayList<String> finarr = new ArrayList<String>();
		for(int i = 0; i < s.length; i++){
			String c = s[i].trim();
			if(c.equals(""))
				continue;
			if(c.contains(",")){
				String[] spt = c.split(",");
				String lreg = spt[0].trim();
				if(lreg != "")
					finarr.add(lreg);
				if(spt.length > 1){
					String rreg = spt[1].trim();
					if(rreg != "")
						finarr.add(rreg);
				}
				continue;
			}
			finarr.add(c);
		}
		String[] ret = new String[finarr.size()];
		finarr.toArray(ret);
		return ret;
	}

	public static boolean isLowerCase(String s){
		for(int i = 0; i < s.length(); i++){
			if(!Character.isLowerCase(s.charAt(i)))
				return false;
		}
		return true;
	}

	public static void check_label(ASMMachine v, String cmd, int ln){
		String[] pts = cmd.split(" ");
		for(int i = 0; i < pts.length; i++){
			if(pts[i] == "")
				continue;
			if(isLowerCase(pts[i])){
				v.labels.put(pts[i], ln);
			}
			break;
		}
	}

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);

		int memsize = parse_hex(sc.nextLine());

		ASMMachine v = new ASMMachine(memsize + 1);

		ArrayList<Command> instrs = new ArrayList<Command>();
		String cmd = sc.nextLine();
		int ln = 0;
		check_label(v, cmd, ln);
		while(sc.hasNextLine()){
			ln ++;
			instrs.add(parse_cmd(cmd));
			cmd = sc.nextLine();
			check_label(v, cmd, ln);
		}
		instrs.add(parse_cmd(cmd));
		while(v.ip < instrs.size()){
			v.exec(instrs.get(v.ip));
		}
	}

}

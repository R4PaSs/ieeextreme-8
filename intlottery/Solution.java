import java.util.*;

class Solution {

	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		String[] lb = sc.nextLine().trim().split(" ");
		int s = Integer.parseInt(lb[0]);
		int e = Integer.parseInt(lb[1]);
		int p = Integer.parseInt(lb[2]);
		int n = Integer.parseInt(lb[3]);
		LinkedList<String> strs = new LinkedList<String>();
		for(int i = 0; i < n; i++){
			strs.add(sc.nextLine().trim());
		}
		LinkedList<Integer> lst = new LinkedList<Integer>();
		for(int i = s; i <= e; i++){
			String ss = Integer.toString(i);
			for(String k: strs){
				if(ss.contains(k)){
					lst.add(i);
					break;
				}
			}
			if(lst.size() >= p)
				break;
		}
		if (p > lst.size()){
			System.out.println("DOES NOT EXIST");
		}else{
			System.out.println(lst.get(p - 1));
		}
	}
}

import java.util.*;

class Solution {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int sqsize = Integer.parseInt(sc.nextLine().trim());
		int[][] matrix = new int[sqsize][sqsize];
		for(int i = 0; i < sqsize; i++){
			String ln = sc.nextLine().trim();
			String[] dgs = ln.split(" ");
			for(int j = 0; j < dgs.length; j++){
				matrix[i][j] = Integer.parseInt(dgs[j]);
			}
		}
		int diagsum = 0;
		for(int i = 0; i < sqsize; i++){
			diagsum += matrix[i][i];
		}
		int cdiagsum = 0;
		{
			int i = 0;
			for(int j = sqsize-1; j >= 0; j--){
				cdiagsum += matrix[j][i];
				i++;
			}
		}
		int[] rowsums = new int[sqsize];
		for(int i = 0; i < sqsize; i++){
			int currsum = 0;
			for(int j = 0; j < sqsize; j++){
				currsum += matrix[i][j];
			}
			rowsums[i] = currsum;
		}
		int[] colsums = new int[sqsize];
		for(int i = 0; i < sqsize; i++){
			int currsum = 0;
			for(int j = 0; j < sqsize; j++){
				currsum += matrix[j][i];
			}
			colsums[i] = currsum;
		}
		ArrayList<Integer> nomatch = new ArrayList<Integer>();
		for(int i = rowsums.length - 1; i >= 0 ; i--){
			if(rowsums[i] != diagsum){
				nomatch.add(i + 1);
			}
		}
		if(cdiagsum != diagsum)
			nomatch.add(0);
		for(int i = 0; i < colsums.length; i++){
			if(colsums[i] != diagsum){
				nomatch.add(-i - 1);
			}
		}
		System.out.println(nomatch.size());
		for(int i = nomatch.size() - 1; i >= 0; i--){
			System.out.println(nomatch.get(i));
		}
	}

}
